/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primos;

/**
 *
 * @author Anthony Mauricio
 */
public class NumerosPrimos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        NumerosPrimos p1 = new NumerosPrimos();
        System.out.println("Los Numeros Primos: " + p1.primos(100));
    }

    public String primos(int numero) {
        String primos = "";
        int cont = 0;
        for (int i = 1; i <= numero; i++) {
            if (isPrimo(i) && cont == 0) {
                primos = i + "";
                cont++;

            } else if (isPrimo(i) && cont < 10) {
                primos = primos + " ," + i;
                cont++;
            }
        }
        int contador = 2;
        boolean primo = true;
        while ((primo) && (contador != numero)) {
            if (numero % contador == 0) {
                primo = false;
            }
            contador++;
        }
        return primos;
    }

    public static boolean isPrimo(int numero) {
        int contador = 2;
        boolean primo = true;
        while ((primo) && (contador != numero)) {
            if (numero % contador == 0) {
                primo = false;
            }
            contador++;
        }
        return primo;

    }

}
